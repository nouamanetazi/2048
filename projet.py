from tkinter import *
from random import choices, choice

def valeur(i,j):
    return can.itemcget(tableau[i][j], 'text')
def update(i,j,txt):
    can.itemconfigure(tableau[i][j], text=txt)
def merge(l1,l2):
    global score_label
    (i1,j1)=l1
    (i2,j2)=l2
    try:
        update(i1,j1,str( int( valeur(i1,j1) ) *2 ) )
        actual_score= can.itemcget(score_label, 'text')[7:]
        can.itemconfigure(score_label, text="Score: "+str(int(actual_score) + int( valeur(i1,j1) )))
    except:
        update(i1,j1,"")
    update(i2,j2,"")
def fusionner_droite():
    change=False #change=fusion_possible
    for i in range(4):
        for j in range(3,0,-1):
            if valeur(i,j)!="":
                l=j-1
                if valeur(i,l)==valeur(i,j):
                    merge((i,j),(i,l)) # to i,j
                    change=True
                    continue
                while valeur(i,l)=="" and l>=1:
                    if valeur(i,l-1)==valeur(i,j):
                        merge((i,j),(i,l-1))
                        change=True
                        break
                    l -= 1
    return change
def fusionner_gauche():
    change=False
    for i in range(4):
        for j in range(3):
            if valeur(i,j)!="":
                l=j+1
                if valeur(i,l)==valeur(i,j):
                    merge((i,j),(i,l)) # to i,j
                    continue
                    change=True
                while valeur(i,l)=="" and l<=2:
                    if valeur(i,l+1)==valeur(i,j):
                        merge((i,j),(i,l+1))
                        break
                        change=True
                    l += 1
    return change
def fusionner_haut():
    change=False
    for j in range(4):
        for i in range(3):
            if valeur(i,j)!="":
                k=i+1
                if valeur(k,j)==valeur(i,j):
                    merge((i,j),(k,j)) # to i,j
                    change=True
                    continue
                while valeur(k,j)=="" and k<=2:
                    if valeur(k+1,j)==valeur(i,j):
                        merge((i,j),(k+1,j))
                        change=True
                        break
                    k += 1
    return change
def fusionner_bas():
    change=False
    for j in range(4):
        for i in range(3,0,-1):
            if valeur(i,j)!="":
                k=i-1
                if valeur(k,j)==valeur(i,j):
                    merge((i,j),(k,j)) # to i,j
                    change=True
                    continue
                while valeur(k,j)=="" and k >= 1:
                    if valeur(k-1,j)==valeur(i,j):
                        merge((i,j),(k-1,j))
                        change=True
                        break
                    k -= 1
    return change

def possible_deplacer(direction):
    # print(can.itemcget(tableau[2][3], 'text'))
    if direction=="droite":
        dir_i , dir_j = 0 , 1
    elif direction == "gauche":
        dir_i, dir_j = 0, -1
    elif direction == "bas":
        dir_i, dir_j = 1, 0
    elif direction == "haut":
        dir_i, dir_j = -1, 0

    for i in range(4):
        for j in range(4):
            if 0<=j+dir_j<=3 and 0<=i+dir_i<=3 and valeur(i,j)!="" and valeur(i+dir_i,j+dir_j)=="":
                return True
    return False


def deplacer(direction): # cas de 4 4 4
    if direction=="droite":
        dir_i , dir_j = 0 , 1
    elif direction == "gauche":
        dir_i, dir_j = 0, -1
    elif direction == "bas":
        dir_i, dir_j = 1, 0
    elif direction == "haut":
        dir_i, dir_j = -1, 0

    for i in range(4):
        for j in range(4):
            if 0 <= j + dir_j <= 3 and 0 <= i + dir_i <= 3:
                if valeur(i,j)!="" and valeur(i+dir_i , j+dir_j)=="":
                    update(i+dir_i,j+dir_j,valeur(i,j))
                    update(i,j,"")

def generer_nouveau_bloc(): #genere 2 ou 4 aléatoirement (majoritairement 2)
    L=[]
    for i in range(4):
        for j in range(4):
            if valeur(i,j)=="":
                L.append((i,j))
    (i,j)=choice(L)
    [number]=choices([2,4], [0.9,0.1])
    update(i,j,str(number))

def restart():
    global score_label,bestscore_label
    for i in range(4):
        for j in range(4):
            update(i,j,"")
    generer_nouveau_bloc()
    best=max(int(can.itemcget(score_label, 'text')[7:]),int(can.itemcget(bestscore_label, 'text')[6:]))
    can.itemconfigure(bestscore_label, text="Best: "+str(best))
    can.itemconfigure(score_label, text="Score: 0")
    colorer()

def savegame():
    global score_label
    L=[]
    for i in range(4):
        for j in range(4):
            L.append(valeur(i,j))
    previous_games.append(L)
    previous_scores.append(can.itemcget(score_label, 'text'))

def undo():
    global score_label
    L=previous_games.pop()
    L=previous_games[-1].copy()
    for i in range(3,-1,-1):
        for j in range(3,-1,-1):
            update(i,j,L.pop())
    score=previous_scores.pop()
    score=previous_scores[-1]
    can.itemconfigure(score_label, text=score)
    colorer()

def colorer():
    for i in range(len(labels)):
        num=can.itemcget(labels[i], 'text')
        if num=="": color="#CDC1B5"
        elif num=="2": color="#EEE4DA"
        elif num=="4": color="#ECE0CA"
        elif num=="8": color="#F2B179"
        elif num=="16": color='#EE976A'
        elif num=="32": color="#F87C5B"
        elif num=="64": color="#FA5C3F"
        elif num=="128": color="#EDCE70"
        elif num=="256": color="#EECC63"
        elif num=="512": color="#EEC657"
        elif num=="1024": color="#F0C345"
        elif num=="2048": color="#EFBE36"
        can.itemconfig(tiles[i],fill=color)
        if num=="" or int(num)<5: can.itemconfig(labels[i],fill='#777066')
        else: can.itemconfig(labels[i],fill='white')


main = Tk()
can = Canvas(main, bg="#FBF8F1", height=400, width=500)

labels,tiles = [],[]
for i in range(16):
    X = (i * 80) % 320
    Y = (i // 4) * 80
    tile=can.create_rectangle(X, Y, X + 80, Y + 80, fill="#CDC1B5", outline="#BAAF9E", width=9)
    mylabel = can.create_text((X + 40, Y + 40), text="",font=("Arial", 30))
    tiles.append(tile)
    labels.append(mylabel)
tableau = [[labels[i] for i in range(4)], [labels[i] for i in range(4, 8)], [labels[i] for i in range(8, 12)],
           [labels[i] for i in range(12, 16)]]

score_label = can.create_text((80, 350), text="Score: 0",font=("Arial", 16))
bestscore_label = can.create_text((240, 350), text="Best: 0",font=("Arial", 16))
can.create_text((240, 380), text="Use Arrows Keys to move the tiles.",font=("Arial", 16))
previous_games=[]
previous_scores=[]

generer_nouveau_bloc()
colorer()

def leftKey(event):
    if fusionner_gauche() or possible_deplacer("gauche"):
        while possible_deplacer("gauche"):
            deplacer("gauche")
        generer_nouveau_bloc()
        savegame()
        colorer()
def rightKey(event):
    if fusionner_droite() or possible_deplacer("droite"):
        while possible_deplacer("droite"):
            deplacer("droite")
        generer_nouveau_bloc()
        savegame()
        colorer()
def upKey(event):
    if fusionner_haut() or possible_deplacer("haut"):
        while possible_deplacer("haut"):
            deplacer("haut")
        generer_nouveau_bloc()
        savegame()
        colorer()
def downKey(event):
    if fusionner_bas() or possible_deplacer("bas"):
        while possible_deplacer("bas"):
            deplacer("bas")
        generer_nouveau_bloc()
        savegame()
        colorer()

button1 = Button(can, text = "Restart",font=("Arial", 16), command = restart, anchor = CENTER)
button1.configure(width = 10, background="#BAAF9E", activebackground = "#33B5E5", relief = FLAT)
button1_window = can.create_window(350, 100, anchor=NW, window=button1)
button2 = Button(can, text = "Undo",font=("Arial", 16), command = undo, anchor = CENTER)
button2.configure(width = 10, background="#BAAF9E", activebackground = "#33B5E5", relief = FLAT)
button2_window = can.create_window(350, 180, anchor=NW, window=button2)
can.pack()
main.bind('<Left>', leftKey)
main.bind('<Right>', rightKey)
main.bind('<Up>', upKey)
main.bind('<Down>', downKey)

main.iconbitmap('2048.ico')
main.title("2048")
main.mainloop()

